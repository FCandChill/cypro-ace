﻿using DiffMatchPatch;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Linq;

namespace Cypro_Ace
{
    public partial class Form1 : Form
    {
        private XDocument
            X_Original,
            X_New,
            X_Menu,
            X_Comment,
            X_Settings,
            X_Proof;

        private string
            path_original,
            path_new,
            path_menu,
            path_comment,
            path_settings,
            path_proof;

        bool Proof_Path_Exists;

        public BindingList<string> Menu_items;
        public List<FindNReplace> L_FindNReplace;
        private const int PER_PAGE = 10;
        private const string SEPERATOR = " - ";
        private string Body = "";
        private int NumberOfEntries;
        private int[] LineNumber;

        private string WriteScriptLocation;

        private enum Columns
        {
            Index = 1,
            Name,
            Original,
            Original_REGEX,
            New,
            Proofread,
            Comment
        }

        public string NewTextbox
        {
            get => ((System.Windows.Controls.TextBox)elementHost1.Child).Text;
            set => ((System.Windows.Controls.TextBox)elementHost1.Child).Text = value;
        }

        public Form1()
        {
            InitializeComponent();

            System.Windows.Controls.TextBox richTextBox_new = new System.Windows.Controls.TextBox();
            richTextBox_new.Name = "richTextBox_new";
            richTextBox_new.SpellCheck.IsEnabled = true;
            richTextBox_new.VerticalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Visible;
            richTextBox_new.FontFamily = new System.Windows.Media.FontFamily("Courier New");
            richTextBox_new.FontSize = 14F;
            richTextBox_new.TextWrapping = System.Windows.TextWrapping.Wrap;
            elementHost1.Child = richTextBox_new;

            System.Windows.Controls.TextBox richTextBox_proof = new System.Windows.Controls.TextBox();
            richTextBox_proof.Name = "richTextBox_proof";
            richTextBox_proof.SpellCheck.IsEnabled = true;
            richTextBox_proof.IsReadOnly = true;
            richTextBox_proof.VerticalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Visible;
            richTextBox_proof.FontFamily = new System.Windows.Media.FontFamily("Courier New");
            richTextBox_proof.FontSize = 14F;
            richTextBox_proof.TextWrapping = System.Windows.TextWrapping.Wrap;
            elementHost2.Child = richTextBox_proof;

        }

        private void listBox_menu_SelectedIndexChanged(object sender = null, EventArgs e = null)
        {
#if (!DEBUG)
            try
            {
#endif
            int i = listBox_menu.SelectedIndex;
                string text = listBox_menu.GetItemText(listBox_menu.SelectedItem);

                if (text != string.Empty)
                {
                    int IndexOfSeperator = text.IndexOf(SEPERATOR) + SEPERATOR.Length;

                    textBox_name.Text = text.Substring(IndexOfSeperator, text.Length - IndexOfSeperator);

                    int IndexOfText = int.Parse(text.Substring(0, PaddingLength));

                    richTextBox_original.Text = FindElement(IndexOfText, X_Original);
                    SetTextFromNewTextBox(FindElement(IndexOfText, X_New));

                    if (Proof_Path_Exists)
                    {
                        SetTextFromProofTextBox(FindElement(IndexOfText, X_Proof));
                    }

                    richTextBox_comment.Text = FindElement(IndexOfText, X_Comment);
                    Update_preview();
                }

#if (!DEBUG)



        }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
#endif
        }

        private string FindElement(int IndexOfText, XDocument xdoc)
        {
            string EntryName = "";
            foreach (XElement X_Entry in xdoc.Element(Constants.ROOT).Elements())
            {
                if (int.Parse(X_Entry.Attribute(Constants.NUMBER).Value) == EntryNumber)
                {
                    foreach (XElement Script_Entry in X_Entry.Elements())
                    {
                        if (Script_Entry.Attribute(Constants.NUMBER) == null)
                        {
                            throw new Exception("No \"no\" tag in xdoc. Adjust the XML files manually to prevent data corruption.");
                        }

                        if (int.Parse(Script_Entry.Attribute(Constants.NUMBER).Value) == IndexOfText)
                        {
                            EntryName = Script_Entry.Value;
                            EntryName = EntryName.Replace("\r", "");
                            EntryName = EntryName.Replace("\n", Environment.NewLine);

                            goto GetOut;
                        }
                    }
                }
            }

            EntryName = "does not exist";
            //throw new Exception();
        GetOut:;

            return EntryName;
        }

        private void SaveElement(string EntryValue, int IndexOfText, ref XDocument xdoc, bool preserveLineBreak)
        {
            if (!preserveLineBreak)
            {
                EntryValue = EntryValue.Replace("\r", string.Empty).Replace("\n", string.Empty);
            }

            foreach (XElement X_Entry in xdoc.Element(Constants.ROOT).Elements())
            {
                if (int.Parse(X_Entry.Attribute(Constants.NUMBER).Value) == EntryNumber)
                {
                    foreach (XElement Script_Entry in X_Entry.Elements())
                    {
                        if (int.Parse(Script_Entry.Attribute(Constants.NUMBER).Value) == IndexOfText)
                        {
                            Script_Entry.Value = EntryValue;
                            goto GetOut;
                        }
                    }
                }
            }
            throw new Exception();
        GetOut:;
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
#if (!DEBUG)
            try
            {
#endif
            using (OpenFileDialog FD = new OpenFileDialog() { Filter = "CYPRO|*.cypro" })
                {
                    if (FD.ShowDialog() == DialogResult.OK)
                    {
                        if (File.Exists(FD.FileName))
                        {
                            NumberOfEntries = 0;

                            Body = Path.GetDirectoryName(FD.FileName);

                            path_original = Path.Combine(Body, "Original.xml");
                            path_new = Path.Combine(Body, "New.xml");
                            path_proof = Path.Combine(Body, "Proof.xml");

                            if (!(Proof_Path_Exists = File.Exists(path_proof)))
                            {
                                splitContainer2.Panel2Collapsed = true;
                                splitContainer2.Panel2.Hide();
                                checkBox_proofread.Visible = false;
                                X_Proof = null;
                            }
                            else
                            {
                                X_Proof = XDocument.Load(path_proof, LoadOptions.PreserveWhitespace);

                                splitContainer2.Panel2Collapsed = false;
                                splitContainer2.Panel2.Show();
                                checkBox_proofread.Visible = true;
                            }

                            path_menu = Path.Combine(Body, "Menu.xml");
                            path_comment = Path.Combine(Body, "Comment.xml");
                            path_settings = Path.Combine(Body, "Main.cypro");

                            X_Original = XDocument.Load(path_original, LoadOptions.PreserveWhitespace);
                            X_New = XDocument.Load(path_new, LoadOptions.PreserveWhitespace);
                            X_Menu = XDocument.Load(path_menu, LoadOptions.PreserveWhitespace);
                            X_Comment = XDocument.Load(path_comment, LoadOptions.PreserveWhitespace);
                            X_Settings = XDocument.Load(path_settings, LoadOptions.PreserveWhitespace);

                            List<int> Linenumber_ = new List<int>();
                            foreach (XElement Entry in X_New.Element(Constants.ROOT).Elements())
                            {
                                int NumberOfLines = 0;
                                foreach (XElement e_tag in Entry.Elements())
                                {
                                    e_tag.Attribute("no").SetValue(NumberOfLines);

                                    NumberOfLines++;
                                }

                                Linenumber_.Add(NumberOfLines);
                            }

                            foreach (int i in Linenumber_)
                            {
                                NumberOfEntries += i;
                            }

                            LineNumber = Linenumber_.ToArray();

                            //load find and replace settings.

                            L_FindNReplace = new List<FindNReplace>();
                            foreach (XElement a in X_Settings.Element(Constants.ROOT).Element("ScriptManager").Element("DisplayReplaceCollection").Elements())
                            {
                                string Find = a.Element("Find").Value;
                                string Replace = a.Element("Replace") == null ? "" : a.Element("Replace").Value;
                                string Replace_New = a.Element("Replace_New") == null ? "" : a.Element("Replace_New").Value;
                                string Replace_Old = a.Element("Replace_Old") == null ? "" : a.Element("Replace_Old").Value;

                                L_FindNReplace.Add(new FindNReplace(Find, Replace, Replace_New, Replace_Old));
                            }

                            XElement scriptEle = X_Settings.Element(Constants.ROOT).Element("Options").Element("WriteScript");

                            if (scriptEle != null)
                            {
                                WriteScriptLocation = scriptEle.Value;
                            }
                            else
                            {
                                writeAndLaunchGameToolStripMenuItem.Visible = false;
                            }

                            numericUpDown_entry.Maximum = LineNumber.Length - 1;
                            numericUpDown_entry_ValueChanged();

                            splitContainer_main.Enabled = true;
                            spreadsheetToolStripMenuItem.Enabled = true;
                            saveToDiskToolStripMenuItem.Enabled = true;
                            otherToolStripMenuItem.Enabled = true;
                        }
                        else throw new Exception("File doesn't exist.");
                    }
                }

#if (!DEBUG)
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
#endif
        }

        private void SplitContainer_original_SplitterMoved(object sender, SplitterEventArgs e)
        {
            splitContainer_proof.SplitterDistance = splitContainer_original.SplitterDistance;
        }


        private void SplitContainer_proof_SplitterMoved(object sender, SplitterEventArgs e)
        {
            splitContainer_new.SplitterDistance = splitContainer_proof.SplitterDistance;
        }

        private void splitContainer_new_SplitterMoved(object sender, SplitterEventArgs e)
        {
            splitContainer_original.SplitterDistance = splitContainer_new.SplitterDistance;
        }
        private void numericUpDown_entry_ValueChanged(object sender = null, EventArgs e = null)
        {
            decimal newmax = Math.Ceiling(LineNumber[EntryNumber] / (decimal)PER_PAGE) - 1;

            Search_For_Dialogue_In_XML();
        }

        private void button_edit_name_Click(object sender, EventArgs e)
        {
            int i = listBox_menu.SelectedIndex;
            string text = listBox_menu.GetItemText(listBox_menu.SelectedItem);
            string menu_name = textBox_name.Text;
            int index = int.Parse(text.Substring(0, PaddingLength));

            Menu_items[i] = FormatMenuItem(index, menu_name);

            SaveElement(menu_name, index, ref X_Menu, preserveLineBreak: false);
        }

        public int PaddingLength { get => LineNumber[EntryNumber].ToString().Length; }

        private void Search_For_Dialogue_In_XML(object sender = null, EventArgs e = null)
        {
#if (!DEBUG)
            try
            {
#endif
            XDocument xdoc = null;
                bool IsScript = false;
                bool IsMenu = false;

                string SearchText = textBox_search.Text.ToLower();
                bool UseRegex = checkBox_regex.Checked;

                int Entry_Index = (int)numericUpDown_entry.Value;

                List<int> Id_matched = new List<int>();

                bool IsProofread = checkBox_proofread.Checked;

                if (radioButton_menu.Checked)
                {
                    xdoc = X_Menu;
                    IsMenu = true; 
                }
                else if (radioButton_Comment.Checked)
                {
                    xdoc = X_Comment;
                }
                else if (radioButton_original.Checked)
                {
                    xdoc = X_Original;
                    IsScript = true;
                }
                else if (radioButton_new.Checked)
                {
                    xdoc = X_New;
                    IsScript = true;
                }

                int Entry_index = 0;
                foreach (XElement EntryTags in xdoc.Element(Constants.ROOT).Elements())
                {
                    if (Entry_index == Entry_Index)
                    {
                        int E_index = 0;

                        progressbar.Maximum = EntryTags.Descendants().Count();

                        foreach (XElement DialogueEntry in EntryTags.Elements())
                        {
                            string EntryString;
                            if (!IsMenu)
                            {
                                EntryString = DialogueEntry.Value;
                            }
                            else
                            {
                                EntryString = FormatMenuItem(E_index, DialogueEntry.Value);
                            }

                            if (IsScript)
                            {
                                EntryString = CreatePreview(EntryString, radioButton_new.Checked);
                            }

                            if (!UseRegex)
                            {
                                EntryString = EntryString.ToLower();
                            }

                            bool pass = false;
                            if (UseRegex)
                            {
                                Regex regex = new Regex(SearchText);
                                Match match = regex.Match(EntryString);

                                if (match.Success)
                                {
                                    pass = true;
                                }
                            }
                            else
                            {
                                if (EntryString.Contains(SearchText))
                                {
                                    pass = true;
                                }
                            }

                            if (pass)
                            {
                                if (IsProofread)
                                {
                                    if ((FindElement(E_index, X_New) == FindElement(E_index, X_Proof)))
                                    {
                                        Id_matched.Add(E_index);
                                    }
                                }
                                else
                                {
                                    Id_matched.Add(E_index);
                                }
                            }

                            E_index++;
                            progressbar.Increment(1);
                        }
                        break;
                    }
                    Entry_index++;
                }

                Menu_items = new BindingList<string>();

                int i = 0;
                int[] Id_a = Id_matched.ToArray();

                foreach (string MenuItem in ReadEntriesFromList(Id_matched, Entry_Index, X_Menu))
                {
                    Menu_items.Add(FormatMenuItem(Id_a[i], MenuItem));
                    i++;
                }

                listBox_menu.DataSource = Menu_items;

                if (listBox_menu.Items.Count == 0)
                {
                    richTextBox_original.Text =
                    richTextBox_preview_original.Text =

                    richTextBox_preview_proof.Text =
                    richTextBox_preview_new.Text =
                    textBox_name.Text =
                    richTextBox_comment.Text = "";
                    SetTextFromNewTextBox("");
                    SetTextFromProofTextBox("");
                    SetEnabilityForRichTextboxes(false);

                    textBox_name.Enabled = button_edit_name.Enabled = button_save_to_RAM.Enabled = false;
                }
                else
                {
                    textBox_name.Enabled = button_edit_name.Enabled = button_save_to_RAM.Enabled = true;
                    SetEnabilityForRichTextboxes(true);
                }
                progressbar.Value = 0;
#if (!DEBUG)

            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
#endif
        }

        private string FormatMenuItem(int id, string MenuText)
        {
            return string.Format("{0}{1}{2}", id.ToString("D" + PaddingLength), SEPERATOR, MenuText);
        }

        private string GetTextFromNewTextBox()
        {
            //https://stackoverflow.com/questions/9650518/getting-textbox-data-from-a-control-hosted-in-winforms
            var elementHost = this.elementHost1;
            var wpfTextBox = (System.Windows.Controls.TextBox)elementHost.Child;
            return wpfTextBox.Text;
        }

        private string GetTextFromProofTextBox()
        {
            //https://stackoverflow.com/questions/9650518/getting-textbox-data-from-a-control-hosted-in-winforms
            var elementHost = this.elementHost2;
            var wpfTextBox = (System.Windows.Controls.TextBox)elementHost.Child;
            return wpfTextBox.Text;
        }

        //Linenumber
        private void importToolStripMenuItem_Click(object sender, EventArgs e)
        {
            progressbar.Maximum = NumberOfEntries * 5;

            FileInfo fi = new FileInfo(Path.Combine(Body, Constants.XLSX_EXPORT));

            string[][] Menu = new string[NumberOfEntries][];
            string[][] New = new string[NumberOfEntries][];
            string[][] Proofread = new string[NumberOfEntries][];
            string[][] Comment = new string[NumberOfEntries][];

            using (ExcelPackage excelPackage = new ExcelPackage(fi))
            {
                int WrkbkIndex = 0;

                foreach (ExcelWorksheet worksheet in excelPackage.Workbook.Worksheets)
                {
                    Menu[WrkbkIndex] = new string[LineNumber[WrkbkIndex]];
                    New[WrkbkIndex] = new string[LineNumber[WrkbkIndex]];
                    Proofread[WrkbkIndex] = new string[LineNumber[WrkbkIndex]];
                    Comment[WrkbkIndex] = new string[LineNumber[WrkbkIndex]];

                    for (int Row = 0; Row < LineNumber[WrkbkIndex]; Row++)
                    {
                        progressbar.Increment(1);

                        //LibreOffice stores empty cells as null.
                        string ReadCell(int RowNo, int Col)
                        {
                            var cell = worksheet.Cells[RowNo + Constants.START_ROW, Col].Value;
                            return cell == null ? "" : cell.ToString();
                        }

                        Menu[WrkbkIndex][Row] = ReadCell(Row, (int)Columns.Name);
                        New[WrkbkIndex][Row] = ReadCell(Row, (int)Columns.New);
                        Comment[WrkbkIndex][Row] = ReadCell(Row, (int)Columns.Comment);

                        if (X_Proof != null)
                        {
                            Proofread[WrkbkIndex][Row] = ReadCell(Row, (int)Columns.Proofread);
                        }
                    }

                    WrkbkIndex++;
                }
            }


            void WriteToXDOC(ref XDocument Xdoc_to_write, string[][] ImportedScript)
            {
                int Tab = 0;
                foreach (XElement EntryTags in Xdoc_to_write.Element(Constants.ROOT).Elements())
                {
                    int Entry_No = 0;
                    foreach (XElement DialogueEntry in EntryTags.Elements())
                    {
                        DialogueEntry.Value = ImportedScript[Tab][Entry_No];
                        Entry_No++;
                        progressbar.Increment(1);
                    }
                    Tab++;
                }
            }

            WriteToXDOC(ref X_Menu, Menu);
            WriteToXDOC(ref X_New, New);

            if (X_Proof != null)
            {
                WriteToXDOC(ref X_Proof, Proofread);
            }

            WriteToXDOC(ref X_Comment, Comment);

            int SelectedIndex = listBox_menu.SelectedIndex;
            listBox_menu_SelectedIndexChanged();
            Search_For_Dialogue_In_XML();

            listBox_menu.SelectedIndex = SelectedIndex;

            MessageBox.Show("Done.");
            progressbar.Value = 0;
        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {
#if (!DEBUG)
            try
            {
#endif

            int multiplier = 6;

            if (Proof_Path_Exists)
            {
                multiplier++;
            }

            progressbar.Maximum = multiplier;
            progressbar.Value = 0;

            ExcelWorksheet worksheet = null;

            using (ExcelPackage excelPackage = new ExcelPackage())
            {
                int WorkbookIndex = 1;
                int Row = 1;

                foreach (XElement EntryTags in X_Original.Element(Constants.ROOT).Elements())
                {
                    int col = 1;

                    worksheet = excelPackage.Workbook.Worksheets.Add(WorkbookIndex.ToString());
                    worksheet.Cells.Style.WrapText = true;

                    worksheet.Column((int)Columns.Original).Width =
                    worksheet.Column((int)Columns.Original_REGEX).Width =
                    worksheet.Column((int)Columns.New).Width =
                    worksheet.Column((int)Columns.Proofread).Width = 50;

                    worksheet.Cells.Style.Font.Name = "Tahoma";
                    worksheet.Cells.Style.Font.Size = 8;

                    worksheet.Cells[Row, col++].Value = "Index";
                    worksheet.Cells[Row, col++].Value = "Name";
                    worksheet.Cells[Row, col++].Value = "Original";
                    worksheet.Cells[Row, col++].Value = "Original W/ Regex";
                    worksheet.Cells[Row, col++].Value = "New";
                    worksheet.Cells[Row, col++].Value = "Proofread";
                    worksheet.Cells[Row, col++].Value = "Comment";

                    //

                    WorkbookIndex++;
                }

                Row++;

                void Write(int row_, int column_, XDocument Dox)
                {
                    int Column_i = column_;

                    List<List<string>> l_FirstCol = new List<List<string>>();
                    List<List<string>> l_OtherCCol = new List<List<string>>();
                    List<List<string>> l_Regex = new List<List<string>>();

                    foreach (XElement EntryTags in Dox.Element(Constants.ROOT).Elements())
                    {
                        List<string> FirstCol_ = new List<string>();
                        List<string> OtherCCol_ = new List<string>();
                        List<string> Regex_ = new List<string>();

                        foreach (XElement DialogueEntry in EntryTags.Elements())
                        {
                            switch (column_)
                            {
                                case (int)Columns.Name:
                                    OtherCCol_.Add(DialogueEntry.Value);
                                    break;
                                case (int)Columns.Index:
                                    FirstCol_.Add(DialogueEntry.Attribute("no").Value);
                                    break;
                                case (int)Columns.Original:
                                    OtherCCol_.Add(DialogueEntry.Value);
                                    Regex_.Add(CreatePreview(DialogueEntry.Value, false));
                                    break;
                                case (int)Columns.Proofread:
                                case (int)Columns.New:
                                case (int)Columns.Comment:
                                    OtherCCol_.Add(DialogueEntry.Value);
                                    break;
                            }
                        }

                        l_FirstCol.Add(FirstCol_);
                        l_OtherCCol.Add(OtherCCol_);
                        l_Regex.Add(Regex_);
                    }

                    void Write_To_Excel(List<List<string>> Data)
                    {
                        int WorkBook = 1;
                        foreach (List<string> EntryTags in Data)
                        {
                            worksheet = excelPackage.Workbook.Worksheets[WorkBook++];
                            worksheet.Cells[row_, Column_i].LoadFromCollection(EntryTags);

                            if (Column_i == (int)Columns.Original_REGEX)
                            {
                                for (int i = 1; i < EntryTags.Count; i++)
                                {
                                    worksheet.Row(i).Height = MeasureTextHeight(worksheet.Cells[i, Column_i].Value.ToString(), worksheet.Cells[i, Column_i].Style.Font, (int)worksheet.Column(Column_i).Width);
                                }
                            }
                        }
                        progressbar.Value++;
                    }

                    switch (column_)
                    {
                        case (int)Columns.Index:
                            Write_To_Excel(l_FirstCol);
                            break;
                        case (int)Columns.Name:
                            Write_To_Excel(l_OtherCCol);
                            break;
                        case (int)Columns.Original:
                            Write_To_Excel(l_OtherCCol);
                            Column_i++;
                            Write_To_Excel(l_Regex);
                            break;
                        case (int)Columns.Proofread:
                            Write_To_Excel(l_OtherCCol);
                            break;
                        case (int)Columns.New:
                            Write_To_Excel(l_OtherCCol);
                            break;
                        case (int)Columns.Comment:
                            Write_To_Excel(l_OtherCCol);
                            break;
                    }
                }

                Write(Row, (int)Columns.Index, X_Menu);
                Write(Row, (int)Columns.Name, X_Menu);
                Write(Row, (int)Columns.Original, X_Original);
                Write(Row, (int)Columns.New, X_New);

                if (X_Proof != null)
                {
                    Write(Row, (int)Columns.Proofread, X_Proof);
                }

                Write(Row, (int)Columns.Comment, X_Comment);

                File.Delete(Path.Combine(Body, Constants.XLSX_EXPORT));
                FileInfo fi = new FileInfo(Path.Combine(Body, Constants.XLSX_EXPORT));
                excelPackage.SaveAs(fi);

                MessageBox.Show(string.Format("Exported to {0}.", fi.Directory.ToString()));

                progressbar.Value = 0;
            }
#if (!DEBUG)
            }
            catch (Exception ee)
            {
                progressbar.Value = 0;
                MessageBox.Show(ee.Message);
            }
#endif
        }


        //https://stackoverflow.com/questions/41639278/autofit-row-height-of-merged-cell-in-epplus
        public double MeasureTextHeight(string text, ExcelFont font, int width)
        {
            if (string.IsNullOrEmpty(text)) return 0.0;
            var bitmap = new Bitmap(1, 1);
            var graphics = Graphics.FromImage(bitmap);

            var pixelWidth = Convert.ToInt32(width * 7.5);  //7.5 pixels per excel column width
            var drawingFont = new Font(font.Name, font.Size);
            var size = graphics.MeasureString(text, drawingFont, pixelWidth);

            //72 DPI and 96 points per inch.  Excel height in points with max of 409 per Excel requirements.
            return Math.Min(Convert.ToDouble(size.Height) * 72 / 96, 409);
        }

        private void SetEnabilityForRichTextboxes(bool EnableIt)
        {
            var elementHost = this.elementHost1;
            System.Windows.Controls.TextBox wpfTextBox = (System.Windows.Controls.TextBox)elementHost.Child;

            elementHost = this.elementHost2;
            wpfTextBox = (System.Windows.Controls.TextBox)elementHost.Child;
        }

        private void SetTextFromNewTextBox(string Text)
        {
            var elementHost = this.elementHost1;
            System.Windows.Controls.TextBox wpfTextBox = (System.Windows.Controls.TextBox)elementHost.Child;
            wpfTextBox.Text = Text;

            //Make sure the undo doesn't carry over from the previous entry.
            int OriginalValue = wpfTextBox.UndoLimit;
            wpfTextBox.UndoLimit = 0;
            wpfTextBox.UndoLimit = OriginalValue;
        }

        private void SetTextFromProofTextBox(string Text)
        {
            var elementHost = this.elementHost2;
            System.Windows.Controls.TextBox wpfTextBox = (System.Windows.Controls.TextBox)elementHost.Child;
            wpfTextBox.Text = Text;

            //Make sure the undo doesn't carry over from the previous entry.
            int OriginalValue = wpfTextBox.UndoLimit;
            wpfTextBox.UndoLimit = 0;
            wpfTextBox.UndoLimit = OriginalValue;
        }

        private void Button_save_to_RAM_Click(object sender, EventArgs e)
        {
            int i = listBox_menu.SelectedIndex;
            string text = listBox_menu.GetItemText(listBox_menu.SelectedItem);
            string menu_name = textBox_name.Text;
            int index = int.Parse(text.Substring(0, PaddingLength));

            SaveElement(GetTextFromNewTextBox(), index, ref X_New, preserveLineBreak: false);

            SaveElement(richTextBox_comment.Text, index, ref X_Comment, preserveLineBreak: true);

            Update_preview();
        }

        private void Update_preview()
        {
            richTextBox_preview_new.Text = CreatePreview(GetTextFromNewTextBox(), true);
            richTextBox_preview_proof.Text = CreatePreview(GetTextFromProofTextBox(), true);
            richTextBox_preview_original.Text = CreatePreview(richTextBox_original.Text, false);


            if (Proof_Path_Exists)
            {
                highlight_ProofreadAndNew();
            }
        }

        private string CreatePreview(string input, bool IsNnew)
        {
            string Output = input;

            foreach (FindNReplace Entry in L_FindNReplace)
            {
                string Find = Entry.Find;
                string Replace = "";

                if (!string.IsNullOrEmpty(Entry.Replace))
                {
                    Replace = Regex.Unescape(Entry.Replace);
                }
                else

                {
                    if (IsNnew)
                    {
                        Replace = Regex.Unescape(Entry.Replace_new);
                    }
                    else
                    {
                        Replace = Regex.Unescape(Entry.Replace_old);
                    }
                }

                Output = Regex.Replace(Output, Find, Replace);
            }

            List<string> multiplePatterns = new List<string> { @"\{(.*?)\}", @"\((.*?)\)" }; //remove special codes in parethesis.
            Output = Regex.Replace(Output, string.Join("|", multiplePatterns), string.Empty);
            return Output;
        }


        /// <summary>
        /// Changes encodingt to UTF7.
        /// </summary>
        /// <param name="utf8String"></param>
        /// <returns></returns>
        private static string ProperEncoding(string utf8String)
        {
            string propEncodeString = string.Empty;

            byte[] utf8_Bytes = new byte[utf8String.Length];
            for (int i = 0; i < utf8String.Length; ++i)
            {
                utf8_Bytes[i] = (byte)utf8String[i];
            }

            return Encoding.UTF7.GetString(utf8_Bytes, 0, utf8_Bytes.Length);
        }

        private void SaveToDiskToolStripMenuItem_Click(object sender, EventArgs e)
        {
            progressbar.Maximum = 6;
            X_New.Declaration = new XDeclaration("1.0", X_New.Declaration.Encoding, null);
            progressbar.Increment(1);
            X_Menu.Declaration = new XDeclaration("1.0", X_New.Declaration.Encoding, null);
            progressbar.Increment(1);
            X_Comment.Declaration = new XDeclaration("1.0", X_New.Declaration.Encoding, null);
            progressbar.Increment(1);

            X_New.Save(path_new, SaveOptions.None);

            progressbar.Increment(1);
            
            X_Menu.Save(path_menu, SaveOptions.None);
            progressbar.Increment(1);

            X_Comment.Save(path_comment, SaveOptions.None);
            progressbar.Increment(1);

            MessageBox.Show("Saved successfully,", "Saved");
            progressbar.Value = 0;
        }

        public int EntryNumber { get => (int)numericUpDown_entry.Value; }

        private List<string> ReadEntriesBetweenRange(int Entry_index, int Page, XDocument doc)
        {
            int Start_entry = Page * PER_PAGE;
            int End_Entry = Start_entry + PER_PAGE + 1;

            List<string> str = new List<string>();

            bool found = false;

            for (int i = 0; i < PER_PAGE && Start_entry < LineNumber[Entry_index]; i++)
            {
                int E_index = 0;
                foreach (XElement a in doc.Element("root").Elements())
                {
                    if (E_index == Entry_index)
                    {
                        foreach (XElement a11 in a.Elements())
                        {
                            if (!found && a11.Attribute("no").Value == Start_entry.ToString())
                            {
                                found = true;
                            }

                            if (found)
                            {
                                int value = int.Parse(a11.Attribute("no").Value);

                                if (value == End_Entry)
                                {
                                    goto escape;
                                }
                                else
                                {
                                    str.Add(a11.Value);
                                }
                            }
                        }

                        if (found)
                        {
                            goto escape;
                        }
                    }
                    E_index++;
                }
            }

        escape:;
            return str;
        }

        private List<string> ReadEntriesFromList(List<int> ID_list, int Entry_index, XDocument doc)
        {
            /*
             * List assumed to be in sequential order.
             */

            List<string> str = new List<string>();
            int E_index = 0;
            foreach (XElement a in doc.Element(Constants.ROOT).Elements())
            {
                if (E_index == Entry_index)
                {
                    foreach (XElement a11 in a.Elements())
                    {
                        if (ID_list.Count == 0)
                        {
                            goto escape;
                        }

                        if (a11.Attribute("no").Value == ID_list[0].ToString())
                        {
                            str.Add(a11.Value);
                            ID_list.RemoveAt(0);
                        }
                    }
                }
                E_index++;
            }

        escape:;
            return str;
        }

        private void findAndReplaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_findnreplace f = new Form_findnreplace(this);
            f.Show();
        }


        string finalPath;
        private void runWriteScriptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            finalPath = $"\"{Path.Combine(Body, WriteScriptLocation)}\"";

            if (!finalPath.StartsWith(@"//"))
            {
                try
                {
                    ThreadStart childref = new ThreadStart(ExecuteCommand);
                    Thread childThread = new Thread(childref);
                    childThread.Start();
                }
                catch (ThreadAbortException)
                {
                    MessageBox.Show("Thread Abort Exception");
                }
            }
            else
            {
                MessageBox.Show($"Cannot execute script from fileshare: {Body}");
            }
        }

        private void ExecuteCommand()
        {
            writeAndLaunchGameToolStripMenuItem.Enabled = false;
            var processInfo = new ProcessStartInfo("cmd.exe", "/c " + finalPath);
            var process = Process.Start(processInfo);
            process.WaitForExit();
            process.Close();
            writeAndLaunchGameToolStripMenuItem.Enabled = true;
        }

        //https://stackoverflow.com/questions/24887238/how-to-compare-two-rich-text-box-contents-and-highlight-the-characters-that-are

        diff_match_patch DIFF = new diff_match_patch();

        // these are the diffs
        List<Diff> diffs;

        // chunks for formatting the two RTBs:
        List<Chunk> chunklist1;
        List<Chunk> chunklist2;

        // two color lists:
        Color[] colors1 = new Color[3] { Color.LightGreen, Color.LightSalmon, Color.White };
        Color[] colors2 = new Color[3] { Color.LightSalmon, Color.LightGreen, Color.White };


        public struct Chunk
        {
            public int startpos;
            public int length;
            public Color BackColor;
        }


        private void highlight_ProofreadAndNew()
        {
            diffs = DIFF.diff_main(richTextBox_preview_new.Text, richTextBox_preview_proof.Text);
            DIFF.diff_cleanupSemanticLossless(diffs);      // <--- see note !

            chunklist1 = collectChunks(richTextBox_preview_new);
            chunklist2 = collectChunks(richTextBox_preview_proof);

            paintChunks(richTextBox_preview_new, chunklist1);
            paintChunks(richTextBox_preview_proof, chunklist2);

            richTextBox_preview_new.SelectionLength = 0;
            richTextBox_preview_proof.SelectionLength = 0;
        }


        List<Chunk> collectChunks(RichTextBox RTB)
        {
            RTB.Text = "";
            List<Chunk> chunkList = new List<Chunk>();
            foreach (Diff d in diffs)
            {
                if (RTB == richTextBox_preview_proof && d.operation == Operation.DELETE) continue;  // **
                if (RTB == richTextBox_preview_new && d.operation == Operation.INSERT) continue;  // **

                Chunk ch = new Chunk();
                int length = RTB.TextLength;
                RTB.AppendText(d.text);
                ch.startpos = length;
                ch.length = d.text.Length;

                if (d.operation != Operation.EQUAL)
                {
                    ch.BackColor = RTB == richTextBox_preview_new ? colors1[(int)d.operation]
                                               : colors2[(int)d.operation];
                }
                chunkList.Add(ch);
            }
            return chunkList;

        }

        void paintChunks(RichTextBox RTB, List<Chunk> theChunks)
        {
            foreach (Chunk ch in theChunks)
            {
                RTB.Select(ch.startpos, ch.length);
                RTB.SelectionBackColor = ch.BackColor;
            }

        }

    }























}