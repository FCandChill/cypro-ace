﻿namespace Cypro_Ace
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToDiskToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spreadsheetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.findAndReplaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeAndLaunchGameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.richTextBox_original = new System.Windows.Forms.TextBox();
            this.splitContainer_main = new System.Windows.Forms.SplitContainer();
            this.splitContainer2_left_side_pane = new System.Windows.Forms.SplitContainer();
            this.groupBox_select_entry = new System.Windows.Forms.GroupBox();
            this.splitContainer_select_entry = new System.Windows.Forms.SplitContainer();
            this.groupBox_search = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.radioButton_menu = new System.Windows.Forms.RadioButton();
            this.radioButton_new = new System.Windows.Forms.RadioButton();
            this.radioButton_original = new System.Windows.Forms.RadioButton();
            this.radioButton_Comment = new System.Windows.Forms.RadioButton();
            this.checkBox_regex = new System.Windows.Forms.CheckBox();
            this.checkBox_proofread = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.textBox_search = new System.Windows.Forms.TextBox();
            this.button_search = new System.Windows.Forms.Button();
            this.numericUpDown_entry = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.button_edit_name = new System.Windows.Forms.Button();
            this.textBox_name = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.listBox_menu = new System.Windows.Forms.ListBox();
            this.button_save_to_RAM = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.splitContainer_script = new System.Windows.Forms.SplitContainer();
            this.groupBox_original = new System.Windows.Forms.GroupBox();
            this.splitContainer_original = new System.Windows.Forms.SplitContainer();
            this.groupBox_preview_original = new System.Windows.Forms.GroupBox();
            this.richTextBox_preview_original = new System.Windows.Forms.TextBox();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.groupBox_new = new System.Windows.Forms.GroupBox();
            this.splitContainer_new = new System.Windows.Forms.SplitContainer();
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.groupBox_preview_new = new System.Windows.Forms.GroupBox();
            this.richTextBox_preview_new = new System.Windows.Forms.RichTextBox();
            this.groupBox_proof = new System.Windows.Forms.GroupBox();
            this.splitContainer_proof = new System.Windows.Forms.SplitContainer();
            this.elementHost2 = new System.Windows.Forms.Integration.ElementHost();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.richTextBox_preview_proof = new System.Windows.Forms.RichTextBox();
            this.groupBox_comment = new System.Windows.Forms.GroupBox();
            this.richTextBox_comment = new System.Windows.Forms.TextBox();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.progressbar = new System.Windows.Forms.ToolStripProgressBar();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_main)).BeginInit();
            this.splitContainer_main.Panel1.SuspendLayout();
            this.splitContainer_main.Panel2.SuspendLayout();
            this.splitContainer_main.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2_left_side_pane)).BeginInit();
            this.splitContainer2_left_side_pane.Panel1.SuspendLayout();
            this.splitContainer2_left_side_pane.Panel2.SuspendLayout();
            this.splitContainer2_left_side_pane.SuspendLayout();
            this.groupBox_select_entry.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_select_entry)).BeginInit();
            this.splitContainer_select_entry.Panel1.SuspendLayout();
            this.splitContainer_select_entry.Panel2.SuspendLayout();
            this.splitContainer_select_entry.SuspendLayout();
            this.groupBox_search.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_entry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_script)).BeginInit();
            this.splitContainer_script.Panel1.SuspendLayout();
            this.splitContainer_script.Panel2.SuspendLayout();
            this.splitContainer_script.SuspendLayout();
            this.groupBox_original.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_original)).BeginInit();
            this.splitContainer_original.Panel1.SuspendLayout();
            this.splitContainer_original.Panel2.SuspendLayout();
            this.splitContainer_original.SuspendLayout();
            this.groupBox_preview_original.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.groupBox_new.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_new)).BeginInit();
            this.splitContainer_new.Panel1.SuspendLayout();
            this.splitContainer_new.Panel2.SuspendLayout();
            this.splitContainer_new.SuspendLayout();
            this.groupBox_preview_new.SuspendLayout();
            this.groupBox_proof.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_proof)).BeginInit();
            this.splitContainer_proof.Panel1.SuspendLayout();
            this.splitContainer_proof.Panel2.SuspendLayout();
            this.splitContainer_proof.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox_comment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource2)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.spreadsheetToolStripMenuItem,
            this.otherToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(896, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadToolStripMenuItem,
            this.saveToDiskToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.loadToolStripMenuItem.Text = "Load Project";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // saveToDiskToolStripMenuItem
            // 
            this.saveToDiskToolStripMenuItem.Enabled = false;
            this.saveToDiskToolStripMenuItem.Name = "saveToDiskToolStripMenuItem";
            this.saveToDiskToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.saveToDiskToolStripMenuItem.Text = "Save to Disk";
            this.saveToDiskToolStripMenuItem.Click += new System.EventHandler(this.SaveToDiskToolStripMenuItem_Click);
            // 
            // spreadsheetToolStripMenuItem
            // 
            this.spreadsheetToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importToolStripMenuItem,
            this.exportToolStripMenuItem});
            this.spreadsheetToolStripMenuItem.Enabled = false;
            this.spreadsheetToolStripMenuItem.Name = "spreadsheetToolStripMenuItem";
            this.spreadsheetToolStripMenuItem.Size = new System.Drawing.Size(83, 20);
            this.spreadsheetToolStripMenuItem.Text = "Spreadsheet";
            // 
            // importToolStripMenuItem
            // 
            this.importToolStripMenuItem.Name = "importToolStripMenuItem";
            this.importToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.importToolStripMenuItem.Text = "Import";
            this.importToolStripMenuItem.Click += new System.EventHandler(this.importToolStripMenuItem_Click);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.exportToolStripMenuItem.Text = "Export";
            this.exportToolStripMenuItem.Click += new System.EventHandler(this.exportToolStripMenuItem_Click);
            // 
            // otherToolStripMenuItem
            // 
            this.otherToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.findAndReplaceToolStripMenuItem,
            this.writeAndLaunchGameToolStripMenuItem});
            this.otherToolStripMenuItem.Enabled = false;
            this.otherToolStripMenuItem.Name = "otherToolStripMenuItem";
            this.otherToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.otherToolStripMenuItem.Text = "Other";
            // 
            // findAndReplaceToolStripMenuItem
            // 
            this.findAndReplaceToolStripMenuItem.Name = "findAndReplaceToolStripMenuItem";
            this.findAndReplaceToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.findAndReplaceToolStripMenuItem.Text = "Find and Replace";
            this.findAndReplaceToolStripMenuItem.Click += new System.EventHandler(this.findAndReplaceToolStripMenuItem_Click);
            // 
            // writeAndLaunchGameToolStripMenuItem
            // 
            this.writeAndLaunchGameToolStripMenuItem.Name = "writeAndLaunchGameToolStripMenuItem";
            this.writeAndLaunchGameToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.writeAndLaunchGameToolStripMenuItem.Text = "Run Write Script";
            this.writeAndLaunchGameToolStripMenuItem.Click += new System.EventHandler(this.runWriteScriptToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(61, 4);
            // 
            // richTextBox_original
            // 
            this.richTextBox_original.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox_original.Font = new System.Drawing.Font("Courier New", 10F);
            this.richTextBox_original.Location = new System.Drawing.Point(0, 0);
            this.richTextBox_original.Multiline = true;
            this.richTextBox_original.Name = "richTextBox_original";
            this.richTextBox_original.ReadOnly = true;
            this.richTextBox_original.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.richTextBox_original.Size = new System.Drawing.Size(179, 101);
            this.richTextBox_original.TabIndex = 0;
            // 
            // splitContainer_main
            // 
            this.splitContainer_main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer_main.Enabled = false;
            this.splitContainer_main.Location = new System.Drawing.Point(0, 24);
            this.splitContainer_main.Name = "splitContainer_main";
            // 
            // splitContainer_main.Panel1
            // 
            this.splitContainer_main.Panel1.Controls.Add(this.splitContainer2_left_side_pane);
            // 
            // splitContainer_main.Panel2
            // 
            this.splitContainer_main.Panel2.Controls.Add(this.splitContainer1);
            this.splitContainer_main.Size = new System.Drawing.Size(896, 484);
            this.splitContainer_main.SplitterDistance = 281;
            this.splitContainer_main.TabIndex = 6;
            // 
            // splitContainer2_left_side_pane
            // 
            this.splitContainer2_left_side_pane.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2_left_side_pane.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2_left_side_pane.Name = "splitContainer2_left_side_pane";
            this.splitContainer2_left_side_pane.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2_left_side_pane.Panel1
            // 
            this.splitContainer2_left_side_pane.Panel1.Controls.Add(this.groupBox_select_entry);
            // 
            // splitContainer2_left_side_pane.Panel2
            // 
            this.splitContainer2_left_side_pane.Panel2.Controls.Add(this.button_save_to_RAM);
            this.splitContainer2_left_side_pane.Size = new System.Drawing.Size(281, 484);
            this.splitContainer2_left_side_pane.SplitterDistance = 382;
            this.splitContainer2_left_side_pane.TabIndex = 2;
            // 
            // groupBox_select_entry
            // 
            this.groupBox_select_entry.Controls.Add(this.splitContainer_select_entry);
            this.groupBox_select_entry.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox_select_entry.Location = new System.Drawing.Point(0, 0);
            this.groupBox_select_entry.Name = "groupBox_select_entry";
            this.groupBox_select_entry.Size = new System.Drawing.Size(281, 382);
            this.groupBox_select_entry.TabIndex = 1;
            this.groupBox_select_entry.TabStop = false;
            this.groupBox_select_entry.Text = "Select Entry";
            // 
            // splitContainer_select_entry
            // 
            this.splitContainer_select_entry.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer_select_entry.Location = new System.Drawing.Point(3, 16);
            this.splitContainer_select_entry.Name = "splitContainer_select_entry";
            this.splitContainer_select_entry.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer_select_entry.Panel1
            // 
            this.splitContainer_select_entry.Panel1.Controls.Add(this.groupBox_search);
            this.splitContainer_select_entry.Panel1.Controls.Add(this.numericUpDown_entry);
            this.splitContainer_select_entry.Panel1.Controls.Add(this.label3);
            this.splitContainer_select_entry.Panel1.Controls.Add(this.button_edit_name);
            this.splitContainer_select_entry.Panel1.Controls.Add(this.textBox_name);
            this.splitContainer_select_entry.Panel1.Controls.Add(this.label2);
            // 
            // splitContainer_select_entry.Panel2
            // 
            this.splitContainer_select_entry.Panel2.Controls.Add(this.listBox_menu);
            this.splitContainer_select_entry.Size = new System.Drawing.Size(275, 363);
            this.splitContainer_select_entry.SplitterDistance = 175;
            this.splitContainer_select_entry.TabIndex = 1;
            // 
            // groupBox_search
            // 
            this.groupBox_search.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_search.Controls.Add(this.flowLayoutPanel1);
            this.groupBox_search.Location = new System.Drawing.Point(6, 66);
            this.groupBox_search.Name = "groupBox_search";
            this.groupBox_search.Size = new System.Drawing.Size(266, 107);
            this.groupBox_search.TabIndex = 14;
            this.groupBox_search.TabStop = false;
            this.groupBox_search.Text = "Search";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.radioButton_menu);
            this.flowLayoutPanel1.Controls.Add(this.radioButton_new);
            this.flowLayoutPanel1.Controls.Add(this.radioButton_original);
            this.flowLayoutPanel1.Controls.Add(this.radioButton_Comment);
            this.flowLayoutPanel1.Controls.Add(this.checkBox_regex);
            this.flowLayoutPanel1.Controls.Add(this.checkBox_proofread);
            this.flowLayoutPanel1.Controls.Add(this.panel1);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(260, 88);
            this.flowLayoutPanel1.TabIndex = 19;
            // 
            // radioButton_menu
            // 
            this.radioButton_menu.AutoSize = true;
            this.radioButton_menu.Checked = true;
            this.radioButton_menu.Location = new System.Drawing.Point(3, 3);
            this.radioButton_menu.Name = "radioButton_menu";
            this.radioButton_menu.Size = new System.Drawing.Size(52, 17);
            this.radioButton_menu.TabIndex = 11;
            this.radioButton_menu.TabStop = true;
            this.radioButton_menu.Text = "Menu";
            this.radioButton_menu.UseVisualStyleBackColor = true;
            // 
            // radioButton_new
            // 
            this.radioButton_new.AutoSize = true;
            this.radioButton_new.Location = new System.Drawing.Point(61, 3);
            this.radioButton_new.Name = "radioButton_new";
            this.radioButton_new.Size = new System.Drawing.Size(47, 17);
            this.radioButton_new.TabIndex = 13;
            this.radioButton_new.Text = "New";
            this.radioButton_new.UseVisualStyleBackColor = true;
            // 
            // radioButton_original
            // 
            this.radioButton_original.AutoSize = true;
            this.radioButton_original.Location = new System.Drawing.Point(114, 3);
            this.radioButton_original.Name = "radioButton_original";
            this.radioButton_original.Size = new System.Drawing.Size(60, 17);
            this.radioButton_original.TabIndex = 12;
            this.radioButton_original.Text = "Original";
            this.radioButton_original.UseVisualStyleBackColor = true;
            // 
            // radioButton_Comment
            // 
            this.radioButton_Comment.AutoSize = true;
            this.radioButton_Comment.Location = new System.Drawing.Point(180, 3);
            this.radioButton_Comment.Name = "radioButton_Comment";
            this.radioButton_Comment.Size = new System.Drawing.Size(69, 17);
            this.radioButton_Comment.TabIndex = 18;
            this.radioButton_Comment.Text = "Comment";
            this.radioButton_Comment.UseVisualStyleBackColor = true;
            // 
            // checkBox_regex
            // 
            this.checkBox_regex.AutoSize = true;
            this.checkBox_regex.Location = new System.Drawing.Point(3, 26);
            this.checkBox_regex.Name = "checkBox_regex";
            this.checkBox_regex.Size = new System.Drawing.Size(79, 17);
            this.checkBox_regex.TabIndex = 17;
            this.checkBox_regex.Text = "Use Regex";
            this.checkBox_regex.UseVisualStyleBackColor = true;
            // 
            // checkBox_proofread
            // 
            this.checkBox_proofread.AutoSize = true;
            this.checkBox_proofread.Location = new System.Drawing.Point(88, 26);
            this.checkBox_proofread.Name = "checkBox_proofread";
            this.checkBox_proofread.Size = new System.Drawing.Size(72, 17);
            this.checkBox_proofread.TabIndex = 20;
            this.checkBox_proofread.Text = "Proofread";
            this.checkBox_proofread.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.flowLayoutPanel2);
            this.panel1.Location = new System.Drawing.Point(3, 49);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(210, 31);
            this.panel1.TabIndex = 19;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.textBox_search);
            this.flowLayoutPanel2.Controls.Add(this.button_search);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(210, 31);
            this.flowLayoutPanel2.TabIndex = 16;
            // 
            // textBox_search
            // 
            this.textBox_search.Location = new System.Drawing.Point(3, 3);
            this.textBox_search.Name = "textBox_search";
            this.textBox_search.Size = new System.Drawing.Size(122, 20);
            this.textBox_search.TabIndex = 10;
            // 
            // button_search
            // 
            this.button_search.Location = new System.Drawing.Point(131, 3);
            this.button_search.Name = "button_search";
            this.button_search.Size = new System.Drawing.Size(75, 23);
            this.button_search.TabIndex = 15;
            this.button_search.Text = "Search";
            this.button_search.UseVisualStyleBackColor = true;
            this.button_search.Click += new System.EventHandler(this.Search_For_Dialogue_In_XML);
            // 
            // numericUpDown_entry
            // 
            this.numericUpDown_entry.Location = new System.Drawing.Point(59, 40);
            this.numericUpDown_entry.Name = "numericUpDown_entry";
            this.numericUpDown_entry.Size = new System.Drawing.Size(100, 20);
            this.numericUpDown_entry.TabIndex = 6;
            this.numericUpDown_entry.ValueChanged += new System.EventHandler(this.numericUpDown_entry_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Entry:";
            // 
            // button_edit_name
            // 
            this.button_edit_name.Location = new System.Drawing.Point(165, 11);
            this.button_edit_name.Name = "button_edit_name";
            this.button_edit_name.Size = new System.Drawing.Size(75, 23);
            this.button_edit_name.TabIndex = 4;
            this.button_edit_name.Text = "Edit";
            this.button_edit_name.UseVisualStyleBackColor = true;
            this.button_edit_name.Click += new System.EventHandler(this.button_edit_name_Click);
            // 
            // textBox_name
            // 
            this.textBox_name.Location = new System.Drawing.Point(59, 13);
            this.textBox_name.Name = "textBox_name";
            this.textBox_name.Size = new System.Drawing.Size(100, 20);
            this.textBox_name.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Name:";
            // 
            // listBox_menu
            // 
            this.listBox_menu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox_menu.FormattingEnabled = true;
            this.listBox_menu.Location = new System.Drawing.Point(0, 0);
            this.listBox_menu.Name = "listBox_menu";
            this.listBox_menu.Size = new System.Drawing.Size(275, 184);
            this.listBox_menu.TabIndex = 0;
            this.listBox_menu.SelectedIndexChanged += new System.EventHandler(this.listBox_menu_SelectedIndexChanged);
            // 
            // button_save_to_RAM
            // 
            this.button_save_to_RAM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_save_to_RAM.Location = new System.Drawing.Point(0, 0);
            this.button_save_to_RAM.Name = "button_save_to_RAM";
            this.button_save_to_RAM.Size = new System.Drawing.Size(281, 98);
            this.button_save_to_RAM.TabIndex = 15;
            this.button_save_to_RAM.Text = "Save to RAM";
            this.button_save_to_RAM.UseVisualStyleBackColor = true;
            this.button_save_to_RAM.Click += new System.EventHandler(this.Button_save_to_RAM_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox_comment);
            this.splitContainer1.Size = new System.Drawing.Size(611, 484);
            this.splitContainer1.SplitterDistance = 253;
            this.splitContainer1.TabIndex = 7;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.splitContainer_script);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(611, 253);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Script";
            // 
            // splitContainer_script
            // 
            this.splitContainer_script.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer_script.Location = new System.Drawing.Point(3, 16);
            this.splitContainer_script.Name = "splitContainer_script";
            // 
            // splitContainer_script.Panel1
            // 
            this.splitContainer_script.Panel1.Controls.Add(this.groupBox_original);
            // 
            // splitContainer_script.Panel2
            // 
            this.splitContainer_script.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer_script.Size = new System.Drawing.Size(605, 234);
            this.splitContainer_script.SplitterDistance = 185;
            this.splitContainer_script.TabIndex = 0;
            // 
            // groupBox_original
            // 
            this.groupBox_original.Controls.Add(this.splitContainer_original);
            this.groupBox_original.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox_original.Location = new System.Drawing.Point(0, 0);
            this.groupBox_original.Name = "groupBox_original";
            this.groupBox_original.Size = new System.Drawing.Size(185, 234);
            this.groupBox_original.TabIndex = 1;
            this.groupBox_original.TabStop = false;
            this.groupBox_original.Text = "Original";
            // 
            // splitContainer_original
            // 
            this.splitContainer_original.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer_original.Location = new System.Drawing.Point(3, 16);
            this.splitContainer_original.Name = "splitContainer_original";
            this.splitContainer_original.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer_original.Panel1
            // 
            this.splitContainer_original.Panel1.Controls.Add(this.richTextBox_original);
            // 
            // splitContainer_original.Panel2
            // 
            this.splitContainer_original.Panel2.Controls.Add(this.groupBox_preview_original);
            this.splitContainer_original.Size = new System.Drawing.Size(179, 215);
            this.splitContainer_original.SplitterDistance = 101;
            this.splitContainer_original.TabIndex = 0;
            this.splitContainer_original.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.SplitContainer_original_SplitterMoved);
            // 
            // groupBox_preview_original
            // 
            this.groupBox_preview_original.Controls.Add(this.richTextBox_preview_original);
            this.groupBox_preview_original.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox_preview_original.Location = new System.Drawing.Point(0, 0);
            this.groupBox_preview_original.Name = "groupBox_preview_original";
            this.groupBox_preview_original.Size = new System.Drawing.Size(179, 110);
            this.groupBox_preview_original.TabIndex = 1;
            this.groupBox_preview_original.TabStop = false;
            this.groupBox_preview_original.Text = "In-Game Preview";
            // 
            // richTextBox_preview_original
            // 
            this.richTextBox_preview_original.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox_preview_original.Font = new System.Drawing.Font("Courier New", 10F);
            this.richTextBox_preview_original.Location = new System.Drawing.Point(3, 16);
            this.richTextBox_preview_original.Multiline = true;
            this.richTextBox_preview_original.Name = "richTextBox_preview_original";
            this.richTextBox_preview_original.ReadOnly = true;
            this.richTextBox_preview_original.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.richTextBox_preview_original.Size = new System.Drawing.Size(173, 91);
            this.richTextBox_preview_original.TabIndex = 2;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.groupBox_new);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.groupBox_proof);
            this.splitContainer2.Size = new System.Drawing.Size(416, 234);
            this.splitContainer2.SplitterDistance = 213;
            this.splitContainer2.TabIndex = 5;
            // 
            // groupBox_new
            // 
            this.groupBox_new.Controls.Add(this.splitContainer_new);
            this.groupBox_new.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox_new.Location = new System.Drawing.Point(0, 0);
            this.groupBox_new.Name = "groupBox_new";
            this.groupBox_new.Size = new System.Drawing.Size(213, 234);
            this.groupBox_new.TabIndex = 4;
            this.groupBox_new.TabStop = false;
            this.groupBox_new.Text = "New";
            // 
            // splitContainer_new
            // 
            this.splitContainer_new.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer_new.Location = new System.Drawing.Point(3, 16);
            this.splitContainer_new.Name = "splitContainer_new";
            this.splitContainer_new.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer_new.Panel1
            // 
            this.splitContainer_new.Panel1.Controls.Add(this.elementHost1);
            // 
            // splitContainer_new.Panel2
            // 
            this.splitContainer_new.Panel2.Controls.Add(this.groupBox_preview_new);
            this.splitContainer_new.Size = new System.Drawing.Size(207, 215);
            this.splitContainer_new.SplitterDistance = 101;
            this.splitContainer_new.TabIndex = 0;
            this.splitContainer_new.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContainer_new_SplitterMoved);
            // 
            // elementHost1
            // 
            this.elementHost1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.elementHost1.Location = new System.Drawing.Point(0, 0);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(207, 101);
            this.elementHost1.TabIndex = 7;
            this.elementHost1.Text = "elementHost1";
            this.elementHost1.Child = null;
            // 
            // groupBox_preview_new
            // 
            this.groupBox_preview_new.Controls.Add(this.richTextBox_preview_new);
            this.groupBox_preview_new.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox_preview_new.Location = new System.Drawing.Point(0, 0);
            this.groupBox_preview_new.Name = "groupBox_preview_new";
            this.groupBox_preview_new.Size = new System.Drawing.Size(207, 110);
            this.groupBox_preview_new.TabIndex = 0;
            this.groupBox_preview_new.TabStop = false;
            this.groupBox_preview_new.Text = "In-Game Preview";
            // 
            // richTextBox_preview_new
            // 
            this.richTextBox_preview_new.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox_preview_new.Font = new System.Drawing.Font("Courier New", 10F);
            this.richTextBox_preview_new.Location = new System.Drawing.Point(3, 16);
            this.richTextBox_preview_new.Name = "richTextBox_preview_new";
            this.richTextBox_preview_new.ReadOnly = true;
            this.richTextBox_preview_new.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedBoth;
            this.richTextBox_preview_new.Size = new System.Drawing.Size(201, 91);
            this.richTextBox_preview_new.TabIndex = 0;
            this.richTextBox_preview_new.Text = "";
            // 
            // groupBox_proof
            // 
            this.groupBox_proof.Controls.Add(this.splitContainer_proof);
            this.groupBox_proof.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox_proof.Location = new System.Drawing.Point(0, 0);
            this.groupBox_proof.Name = "groupBox_proof";
            this.groupBox_proof.Size = new System.Drawing.Size(199, 234);
            this.groupBox_proof.TabIndex = 5;
            this.groupBox_proof.TabStop = false;
            this.groupBox_proof.Text = "Proofread";
            // 
            // splitContainer_proof
            // 
            this.splitContainer_proof.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer_proof.Location = new System.Drawing.Point(3, 16);
            this.splitContainer_proof.Name = "splitContainer_proof";
            this.splitContainer_proof.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer_proof.Panel1
            // 
            this.splitContainer_proof.Panel1.Controls.Add(this.elementHost2);
            // 
            // splitContainer_proof.Panel2
            // 
            this.splitContainer_proof.Panel2.Controls.Add(this.groupBox3);
            this.splitContainer_proof.Size = new System.Drawing.Size(193, 215);
            this.splitContainer_proof.SplitterDistance = 101;
            this.splitContainer_proof.TabIndex = 0;
            this.splitContainer_proof.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.SplitContainer_proof_SplitterMoved);
            // 
            // elementHost2
            // 
            this.elementHost2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.elementHost2.Location = new System.Drawing.Point(0, 0);
            this.elementHost2.Name = "elementHost2";
            this.elementHost2.Size = new System.Drawing.Size(193, 101);
            this.elementHost2.TabIndex = 7;
            this.elementHost2.Text = "elementHost2";
            this.elementHost2.Child = null;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.richTextBox_preview_proof);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(193, 110);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "In-Game Preview";
            // 
            // richTextBox_preview_proof
            // 
            this.richTextBox_preview_proof.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox_preview_proof.Font = new System.Drawing.Font("Courier New", 10F);
            this.richTextBox_preview_proof.Location = new System.Drawing.Point(3, 16);
            this.richTextBox_preview_proof.Name = "richTextBox_preview_proof";
            this.richTextBox_preview_proof.ReadOnly = true;
            this.richTextBox_preview_proof.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedBoth;
            this.richTextBox_preview_proof.Size = new System.Drawing.Size(187, 91);
            this.richTextBox_preview_proof.TabIndex = 1;
            this.richTextBox_preview_proof.Text = "";
            // 
            // groupBox_comment
            // 
            this.groupBox_comment.Controls.Add(this.richTextBox_comment);
            this.groupBox_comment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox_comment.Location = new System.Drawing.Point(0, 0);
            this.groupBox_comment.Name = "groupBox_comment";
            this.groupBox_comment.Size = new System.Drawing.Size(611, 227);
            this.groupBox_comment.TabIndex = 0;
            this.groupBox_comment.TabStop = false;
            this.groupBox_comment.Text = "Comment";
            // 
            // richTextBox_comment
            // 
            this.richTextBox_comment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox_comment.Font = new System.Drawing.Font("Courier New", 10F);
            this.richTextBox_comment.Location = new System.Drawing.Point(3, 16);
            this.richTextBox_comment.Multiline = true;
            this.richTextBox_comment.Name = "richTextBox_comment";
            this.richTextBox_comment.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.richTextBox_comment.Size = new System.Drawing.Size(605, 208);
            this.richTextBox_comment.TabIndex = 4;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.progressbar});
            this.statusStrip1.Location = new System.Drawing.Point(0, 486);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(896, 22);
            this.statusStrip1.TabIndex = 7;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // progressbar
            // 
            this.progressbar.Name = "progressbar";
            this.progressbar.Size = new System.Drawing.Size(100, 16);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(896, 508);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.splitContainer_main);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(500, 300);
            this.Name = "Form1";
            this.Text = "All-Purpose Script Manager: Cypro Ace";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer_main.Panel1.ResumeLayout(false);
            this.splitContainer_main.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_main)).EndInit();
            this.splitContainer_main.ResumeLayout(false);
            this.splitContainer2_left_side_pane.Panel1.ResumeLayout(false);
            this.splitContainer2_left_side_pane.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2_left_side_pane)).EndInit();
            this.splitContainer2_left_side_pane.ResumeLayout(false);
            this.groupBox_select_entry.ResumeLayout(false);
            this.splitContainer_select_entry.Panel1.ResumeLayout(false);
            this.splitContainer_select_entry.Panel1.PerformLayout();
            this.splitContainer_select_entry.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_select_entry)).EndInit();
            this.splitContainer_select_entry.ResumeLayout(false);
            this.groupBox_search.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_entry)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.splitContainer_script.Panel1.ResumeLayout(false);
            this.splitContainer_script.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_script)).EndInit();
            this.splitContainer_script.ResumeLayout(false);
            this.groupBox_original.ResumeLayout(false);
            this.splitContainer_original.Panel1.ResumeLayout(false);
            this.splitContainer_original.Panel1.PerformLayout();
            this.splitContainer_original.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_original)).EndInit();
            this.splitContainer_original.ResumeLayout(false);
            this.groupBox_preview_original.ResumeLayout(false);
            this.groupBox_preview_original.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.groupBox_new.ResumeLayout(false);
            this.splitContainer_new.Panel1.ResumeLayout(false);
            this.splitContainer_new.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_new)).EndInit();
            this.splitContainer_new.ResumeLayout(false);
            this.groupBox_preview_new.ResumeLayout(false);
            this.groupBox_proof.ResumeLayout(false);
            this.splitContainer_proof.Panel1.ResumeLayout(false);
            this.splitContainer_proof.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_proof)).EndInit();
            this.splitContainer_proof.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox_comment.ResumeLayout(false);
            this.groupBox_comment.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource2)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.TextBox richTextBox_original;
        private System.Windows.Forms.SplitContainer splitContainer_main;
        private System.Windows.Forms.ListBox listBox_menu;
        private System.Windows.Forms.GroupBox groupBox_new;
        private System.Windows.Forms.GroupBox groupBox_original;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox_select_entry;
        private System.Windows.Forms.SplitContainer splitContainer_select_entry;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.Button button_edit_name;
        private System.Windows.Forms.TextBox textBox_name;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.SplitContainer splitContainer_script;
        private System.Windows.Forms.SplitContainer splitContainer_new;
        private System.Windows.Forms.SplitContainer splitContainer_original;
        private System.Windows.Forms.GroupBox groupBox_preview_new;
        private System.Windows.Forms.GroupBox groupBox_preview_original;
        private System.Windows.Forms.NumericUpDown numericUpDown_entry;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox_search;
        private System.Windows.Forms.Button button_search;
        private System.Windows.Forms.RadioButton radioButton_menu;
        private System.Windows.Forms.TextBox textBox_search;
        private System.Windows.Forms.RadioButton radioButton_new;
        private System.Windows.Forms.RadioButton radioButton_original;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button button_save_to_RAM;
        private System.Windows.Forms.GroupBox groupBox_comment;
        private System.Windows.Forms.SplitContainer splitContainer2_left_side_pane;
        private System.Windows.Forms.TextBox richTextBox_comment;
        private System.Windows.Forms.ToolStripMenuItem saveToDiskToolStripMenuItem;
        private System.Windows.Forms.TextBox richTextBox_preview_original;
        private System.Windows.Forms.CheckBox checkBox_regex;
        private System.Windows.Forms.RadioButton radioButton_Comment;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.BindingSource bindingSource2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.ToolStripMenuItem spreadsheetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar progressbar;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.GroupBox groupBox_proof;
        private System.Windows.Forms.SplitContainer splitContainer_proof;
        private System.Windows.Forms.Integration.ElementHost elementHost2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RichTextBox richTextBox_preview_new;
        private System.Windows.Forms.RichTextBox richTextBox_preview_proof;
        private System.Windows.Forms.CheckBox checkBox_proofread;
        private System.Windows.Forms.ToolStripMenuItem otherToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem findAndReplaceToolStripMenuItem;
        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private System.Windows.Forms.ToolStripMenuItem writeAndLaunchGameToolStripMenuItem;
    }
}

