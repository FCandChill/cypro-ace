﻿using System.IO;
using System.Text;

public static class Clean_Table
{
    public static void Clean(string CHR_Path, string DictionaryPath, string ouput)
    {
        string allFile = File.ReadAllText(DictionaryPath, Encoding.Default);
        /*
        using (StreamReader r = new StreamReader(DictionaryPath, Encoding.Default, true))
        {
            while (!r.EndOfStream)
            {
                allFile += r.ReadLine() + "\r\n";
            }
        }*/

        using (StreamReader r = new StreamReader(CHR_Path, Encoding.Default, true))
        {
            while (!r.EndOfStream)
            {
                string LINEW = r.ReadLine();
                string[] arr = LINEW.Split('=');
                string hex = "{" + arr[0] + "}";
                string str = arr[1];

                if (str.Length != 0)
                    allFile = allFile.Replace(str, hex);
            }
        }

        File.Delete(ouput);
        File.WriteAllText(ouput, allFile);
    }

    private static string properEncoding(string utf8String)
    {
        string propEncodeString = string.Empty;

        byte[] utf8_Bytes = new byte[utf8String.Length];
        for (int i = 0; i < utf8String.Length; ++i)
        {
            utf8_Bytes[i] = (byte)utf8String[i];
        }

        return Encoding.UTF8.GetString(utf8_Bytes, 0, utf8_Bytes.Length);
    }
}