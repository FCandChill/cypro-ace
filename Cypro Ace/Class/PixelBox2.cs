﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace Cypro_Ace.Class
{
    public partial class PixelBox2 : PictureBox
    {
        private const int PIXEL = 8;
        private Bitmap preserveFormat;


        #region Initialization
        /// <summary>
        /// Initializes new instance of the <see cref="PixelBox">class.</see>
        /// </summary>


        public PixelBox2()
        { constructor(); }

        public void constructor()
        {
            InterpolationMode = InterpolationMode.NearestNeighbor;
            InitializeComponent();
        }
        
        public PixelBox2(int Width, int Height)
        {
            constructor();
            base.Width = Width;
            base.Height = Height;
        }

        /// <summary>
        /// Have to store the indexed image in an object
        /// because Microsoft is full of crackpots who can't
        /// code and convert to 32BPPRGB given any chance.
        /// </summary>

        public new Bitmap Image
        {
            set
            {
                preserveFormat = value;
                base.Image = Image;
            }

            get
            { return preserveFormat; }
        }

        #endregion
        #region Properties
        /// <value>The interpolation mode.</value>
        [Category("Behavior")]
        [DefaultValue(InterpolationMode.Default)]
        private InterpolationMode InterpolationMode { get; set; }
        #endregion
        protected override void OnPaint(PaintEventArgs pe)
        {
            /*
            if (!temp && BorderStyle == BorderStyle.FixedSingle)
            {
                temp = true;
                Width += scale;
                Height += scale;
            }*/
            pe.Graphics.InterpolationMode = InterpolationMode;
            pe.Graphics.PixelOffsetMode = PixelOffsetMode.Half; //to stop cutoff
            base.OnPaint(pe);
        }

        private double ScaleFactor(int pixel, PixelBox2 selecting = null)
        {
            if (Image != null)
            {
                if (selecting != null)
                    return Math.Ceiling(format(Width, pixel) / format(selecting.Width, pixel));
                else
                    return Math.Ceiling(format(Width, pixel) / format(Image.Width, pixel));
            }
            else throw new NullReferenceException();
        }

        private double format(int a, int pixel)
        { return (Width / pixel * pixel); }
    }

}
