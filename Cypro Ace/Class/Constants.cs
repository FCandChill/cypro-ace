﻿public class Constants
{
    public const string
        ROOT = "Root",
        NUMBER = "no",
        XLSX_EXPORT = "Script.xlsx";

    public const int START_ROW = 2;
}