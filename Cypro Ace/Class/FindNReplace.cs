﻿public struct FindNReplace
{
    public string Find { get; set; }
    public string Replace { get; set; }

    public string Replace_new { get; set; }
    public string Replace_old { get; set; }

    public FindNReplace(string Find, string Replace, string Replace_new, string Replace_old)
    {
        this.Find = Find;
        this.Replace = Replace;

        this.Replace_new = Replace_new;
        this.Replace_old = Replace_old;
    }
}